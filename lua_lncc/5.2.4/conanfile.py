import os
import shutil

from conans import CMake, ConanFile, tools


class LuaConan(ConanFile):
    name = "lua_lncc"
    version = "5.2.4"
    license = "https://www.lua.org/license.html"
    url = "https://www.lua.org/ftp/"
    description = "Lua is a powerful, efficient, lightweight, embeddable scripting language."
    settings = "os", "compiler", "build_type", "arch"
    options = {"build_interpreter": [True, False], "build_compiler": [True, False]}
    default_options = "build_interpreter=False", "build_compiler=False"
    exports_sources = "CMakeLists.txt", "windows/*", "lua-5.2.4.tar.gz"
    generators = "cmake"
    requires = "ninja-kitware/1.7.2@Promob3D_Dependencias/stable","cmake_lncc/3.13.4@Promob3D_Dependencias/stable"
    _source_subfolder = "lua-%s"%version
    _build_subfolder = "build_subfolder"
    user = "Promob3D_Dependencias"
    channel = "stable"
    fileName = "lua-5.2.4.tar.gz"

    @property
    def zip_folder_name(self):
        return "lua-%s" % self.version

    def build_requirements(self):
        if self.settings.os == 'Windows':
                self.build_requires("mingw_lncc/4.9.4@Promob3D_Dependencias/stable")

    def configure(self):
        if self.settings.os == "Windows":
            self.settings.compiler = "gcc"
            self.settings.compiler.version = "4.9"
            self.settings.compiler.libcxx = "libstdc++11"

    def source(self):
        # tools.get("https://www.lua.org/ftp/lua-%s.tar.gz"%self.version)
        tools.unzip(os.path.join(self.source_folder,self.fileName))
        #os.rename("lua-%s"%self.version, self._source_subfolder)
        tools.replace_in_file(os.path.join(self._source_subfolder,"src","luaconf.h"),\
"""#define LUA_USE_READLINE""",\
"""//#define LUA_USE_READLINE""")
        tools.replace_in_file(os.path.join(self._source_subfolder,"src","luaconf.h"),\
"""#define LUA_USE_DLOPEN""",\
"""//#define LUA_USE_DLOPEN""")

    def build(self):
        # self.build_requirements()
        shutil.move("CMakeLists.txt", "%s/CMakeLists.txt"%self._source_subfolder)
        # if self.settings.os == "Windows":
        #     shutil.move("windows", "%s" % self._source_subfolder)
            # self.run(tools.vcvars_command(self.settings))
        cmake = CMake(self, generator="Ninja")

        # if self.options.build_interpreter:
        cmake.definitions["BUILD_INTERPRETER"] = "OFF"
        # if self.options.build_compiler:
        cmake.definitions["BUILD_COMPILER"] = "OFF"
        cmake.definitions["CONAN_DISABLE_CHECK_COMPILER"] = "ON"
        cmake.parallel=0
        cmake.verbose=1
        cmake.configure(source_dir=self._source_subfolder)
        cmake.build()

    def package(self):
        #export_includes = ["lua.h", "lua.hpp", "lualib.h", "lauxlib.h", "luaconf.h"]
        #for name in export_includes:
        self.copy("*.h", dst="include", src=os.path.join(self._source_subfolder, "src"))
        self.copy("*.hpp", dst="include", src=os.path.join(self._source_subfolder, "src"))
        self.copy("*/lua.exe", dst="bin", keep_path=False)
        self.copy("*/luac.exe", dst="bin", keep_path=False)
        self.copy("*/lua", dst="bin", keep_path=False)
        self.copy("*/luac", dst="bin", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("*.so*", dst="lib", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["lua"]
