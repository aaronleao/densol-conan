import os
from conans import ConanFile, AutoToolsBuildEnvironment, tools, MSBuild
import shutil

class PythonConan(ConanFile):
    name = "python2_lncc"
    version = "2.7.15"
    license = "BSD-3-Clause"
    url = "https://www.python.org/ftp/python"
    homepage = "python.org"
    description = ("The fastest malloc we've seen; works particularly well with threads and STL."
                   "Also: thread-friendly heap-checker, heap-profiler, and cpu-profiler.")
    settings = "os", "compiler", "build_type", "arch"
    short_paths = False
    exports_sources = 'distutils.cfg', 'source_windows.zip'
    user = "Promob3D_Dependencias"
    channel = "stable"
    linuxFileName = "Python-2.7.15.tar.xz"
    #windowsFileName = "python-2.7.15.amd64.msi"
    folderName = "Python-2.7.15"

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    #def build_requirements(self):
    #    if self.settings.os == "Windows":
    #        self.build_requires("mingw/4.9.4@Promob3D_Dependencias/stable")
    #        self.build_requires("msys2/20200517@Promob3D_Dependencias/stable")

    #def configure(self):
    #    if self.settings.os == "Windows":
    #        self.settings.compiler = "gcc"
    #        self.settings.compiler.version = "4.9"
    #        self.settings.compiler.libcxx = "libstdc++11"

    def source(self):
        if self.settings.os == "Windows":
            tools.unzip(filename="source_windows.zip", destination=self._source_subfolder)
            os.remove("source_windows.zip") 
        if self.settings.os == "Linux":
            tools.get("%s/%s/%s" %(self.url,self.version, self.linuxFileName ))
            shutil.move(self.folderName, self._source_subfolder)


    _autotools = False

    def _configure_autotools(self):
        if not self._autotools:
            self._autotools = AutoToolsBuildEnvironment(self, win_bash=tools.os_info.is_windows)
            #self._autotools = AutoToolsBuildEnvironment(self)
            #args = ['--disable-shared', '--enable-static']
            # self._autotools.libs.append("unwind")
            with tools.chdir(self._source_subfolder):
                #self._autotools.configure(args=args)
                self._autotools.configure()

        return self._autotools

    def buildLinux(self):
        autotools = self._configure_autotools()
        with tools.chdir(self._source_subfolder):
            autotools.make()

    #def buildWindows(self):
    #    print("msiexec /i %s INSTALLDIR=\"%s\" "%("python-2.7.15.amd64.msi", self.package_folder))
    #    self.run("msiexec /i %s INSTALLDIR=\"%s\""%("python-2.7.15.amd64.msi", self.package_folder))

    def build(self):
        if self.settings.os == "Windows":
        #    self.buildWindows()
            pass
        if self.settings.os == "Linux":
            self.buildLinux()

    def package(self):
        if self.settings.os == "Windows":
            self.copy("*", src=self._source_subfolder, dst=self.package_folder)
            self.copy("*.exe", src=self._source_subfolder, dst="bin")
            self.copy("*.dll", src=self._source_subfolder, dst="bin")
        #    self.copy("*.cmake", src="src")
        #if self.settings.os == "Linux":
        #    autotools = self._configure_autotools()
        #    with tools.chdir(self._source_subfolder):
        #        autotools.install()
        #    self.copy("*.cmake", src="src")
        #    with tools.chdir(os.path.join(self.package_folder, "lib")):
        #        self.run("rm -f *.la")
        
        if self.settings.os == "Linux":
            autotools = self._configure_autotools()
            with tools.chdir(self._source_subfolder):
                autotools.install()

        #self.copy("*.cmake", src="src")
        #with tools.chdir(os.path.join(self.package_folder, "lib")):
        #    self.run("rm -f *.la")




    #def package(self):
    #    self.copy("*", dst="bin",     src="install/bin",     keep_path=True)
    #    self.copy("*", dst="include", src="install/include", keep_path=True)
    #    self.copy("*", dst="lib",     src="install/lib",     keep_path=True)
    #    self.copy("*", dst="share",   src="install/share",   keep_path=True)

    def package_info(self):
        if self.settings.os == "Windows":
            self.env_info.PATH.append(os.path.join(self.package_folder))
            self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        if self.settings.os == "Linux":
            self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        self.env_info.PYTHONPATH.append(self.package_folder)