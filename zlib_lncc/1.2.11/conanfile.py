import os
import stat
from conans import ConanFile, tools, CMake, AutoToolsBuildEnvironment
from conans.errors import ConanException


class ZlibConan(ConanFile):
    name = "zlib_lncc"
    version = "1.2.11"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://zlib.net"
    license = "Zlib"
    description = ("A Massively Spiffy Yet Delicately Unobtrusive Compression Library "
                   "(Also Free, Not to Mention Unencumbered by Patents)")
    settings = "os", "arch", "compiler", "build_type"
    options = {"shared": [True, False], "fPIC": [True, False], "minizip": [True, False]}
    default_options = {"shared": False, "fPIC": True, "minizip": False}
    exports_sources = ["CMakeLists.txt", "CMakeLists_minizip.txt", "patches/**", "zlib-1.2.11.tar.gz"]
    fileName = "zlib-1.2.11.tar.gz"
    generators = "cmake"
    _source_subfolder = "source_subfolder"
    user = "Promob3D_Dependencias"
    channel = "stable"
    requires = "ninja-kitware/1.7.2@Promob3D_Dependencias/stable",\
    "cmake_lncc/3.13.4@Promob3D_Dependencias/stable"

    def build_requirements(self):
        if self.settings.os == 'Windows':
                self.build_requires("mingw_lncc/4.9.4@Promob3D_Dependencias/stable")

    def configure(self):
        if self.settings.os == "Windows":
            self.settings.compiler = "gcc"
            self.settings.compiler.version = "4.9"
            self.settings.compiler.libcxx = "libstdc++11"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    # def configure(self):
    #     del self.settings.compiler.libcxx
    #     del self.settings.compiler.cppstd

    def source(self):
        tools.unzip(os.path.join(self.source_folder,self.fileName))
        # tools.get(**self.conan_data["sources"][self.version])

        os.rename("{}-{}".format("zlib", self.version), self._source_subfolder)
        if not tools.os_info.is_windows:
            configure_file = os.path.join(self._source_subfolder, "configure")
            st = os.stat(configure_file)
            os.chmod(configure_file, st.st_mode | stat.S_IEXEC)

    def build(self):
        cmake = CMake(self, generator="Ninja")
        cmake.configure(source_folder=self._source_subfolder)
        cmake.build()

    # def _rename_libraries(self):
    #     if self.settings.os == "Windows":
    #         lib_path = os.path.join(self.package_folder, "lib")
    #         suffix = "d" if self.settings.build_type == "Debug" else ""

    #         current_lib = os.path.join(lib_path, "libzlibstatic.a")
    #         os.rename(current_lib, os.path.join(lib_path, "libzlib.a"))


    def package(self):
        # Copy headers
        # tools.chdir(os.path.join(self._source_subfolder))
        for header in ["*zlib.h", "*zconf.h"]:
            self.copy(pattern=header, dst="include", src=self._source_subfolder, keep_path=False)
            self.copy(pattern=header, dst="include", src=self.build_folder, keep_path=False)

        self.copy(pattern="*.a", dst="lib", src=self.build_folder, keep_path=False)
        self.copy(pattern="*.lib", dst="lib", src=self.build_folder, keep_path=False)
        self.copy(pattern="*.dylib*", dst="lib", src=self.build_folder, keep_path=False, symlinks=True)
        self.copy(pattern="*.so*", dst="lib", src=self.build_folder, keep_path=False, symlinks=True)
        self.copy(pattern="*.dll", dst="bin", src=self.build_folder, keep_path=False)
        self.copy(pattern="*.dll.a", dst="lib", src=self.build_folder, keep_path=False)


    def package_info(self):
        if self.options.minizip:
            self.cpp_info.libs.append('minizip')
            if self.options.shared:
                self.cpp_info.defines.append('MINIZIP_DLL')
        self.cpp_info.libs.append('zlib' if self.settings.os == "Windows" else "z")
        # self.cpp_info.names["cmake_find_package"] = "ZLIB"
        # self.cpp_info.names["cmake_find_package_multi"] = "ZLIB"
