from conans import ConanFile, CMake, tools
import subprocess
import os


class NinjaConan(ConanFile):
    name = "ninja-kitware"
    version = "1.7.2"
    license = "Apache License 2.0"
    author = "Aaron aaron@lncc.br"
    url = "https://github.com/Kitware/ninja"
    description = "Donwload prebuilt ninja binary."
    topics = ("builder", "minimalist", "fast")
    settings = "os", "compiler", "build_type", "arch"
    generators = "virtualrunenv"
    user = "Promob3D_Dependencias"
    channel = "stable"
    fileName = ""
    unzipFolder =  ""
    exports_sources = "ninja-1.7.2.gaad58.kitware.dyndep-1_i686-pc-windows-msvc.zip", "ninja-1.7.2.gaad58.kitware.dyndep-1_x86_64-linux-gnu.tar.gz"
    if tools.os_info.is_windows:
        fileName = "ninja-1.7.2.gaad58.kitware.dyndep-1_i686-pc-windows-msvc.zip"
        unzipFolder = "ninja-1.7.2.gaad58.kitware.dyndep-1_i686-pc-windows-msvc"
    if tools.os_info.is_linux:
        fileName = "ninja-1.7.2.gaad58.kitware.dyndep-1_x86_64-linux-gnu.tar.gz"
        unzipFolder = "ninja-1.7.2.gaad58.kitware.dyndep-1_x86_64-linux-gnu"


    def source(self):
        # tools.get("%s/releases/download/v1.7.2.gaad58.kitware.dyndep-1/%s"%(self.url, self.fileName))
        tools.unzip(os.path.join(self.source_folder,self.fileName))
        
    def package(self):
        if tools.os_info.is_linux:
            subprocess.call(['chmod', '+x', "%s/ninja"%(self.unzipFolder)])
            self.copy("ninja", src=os.path.join(self.source_folder,self.unzipFolder), dst="bin")
        if tools.os_info.is_windows:
            self.copy("ninja.exe", src=os.path.join(self.source_folder,self.unzipFolder), dst="bin")
        self.copy("*.sh", src=self.build_folder, dst="bin")
        self.copy("environment*", src=self.build_folder, dst="bin")

    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))

