from conans import ConanFile, CMake, tools
import os

class MumpsConan(ConanFile):
    name = "mumps_lncc"
    version = "5.3.4"
    license = "CeCILL-C license"
    author = "Aaron aaron@lncc.br"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Mumps here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],\
    "fPIC": [True, False],\
    "intsize64":[True, False],\
    "parallel":[True, False],
    "metis":[True, False],
    "scotch":[True, False],
    "openmp":[True, False],
    "autobuild":[True, False]}

    default_options = {"shared": False,\
    "fPIC":      True,\
    "intsize64": False,\
    "parallel":  False,\
    "metis":     False,\
    "scotch":    False,\
    "openmp":    True,\
    "autobuild": False}

    generators = "cmake"
    requires = "ninja-kitware/1.7.2@Promob3D_Dependencias/stable",\
    "cmake_lncc/3.13.4@Promob3D_Dependencias/stable",\
    "lapack_lncc/3.6.1@Promob3D_Dependencias/stable"\
    
    unzipFolder = "mumps-5.3.4.0"
    user = "Promob3D_Dependencias"
    channel = "stable"
    fileName = "mumps-5.3.4.0.zip"
    exports_sources = "mumps-5.3.4.0.zip"
    
    def source(self):
        # self.run("git clone https://github.com/scivision/mumps.git %s"%(self.unzipFolder))
        tools.unzip(os.path.join(self.source_folder,self.fileName))
        tools.replace_in_file(os.path.join(self.unzipFolder,"CMakeLists.txt"), "cmake_minimum_required(VERSION 3.14)", "cmake_minimum_required(VERSION 3.13)")
        tools.replace_in_file(os.path.join(self.unzipFolder,"CMakeLists.txt"),\
"""project(MUMPS
LANGUAGES C Fortran
VERSION %s
DESCRIPTION "Sparse direct parallel solver"
HOMEPAGE_URL "http://mumps-solver.org/")"""%(self.version),\

"""project(MUMPS
LANGUAGES C Fortran
VERSION %s
DESCRIPTION "Sparse direct parallel solver"
HOMEPAGE_URL "http://mumps-solver.org/")\n
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()"""%(self.version))

    def configure(self):
        if self.settings.os == "Windows":
            self.settings.compiler = "gcc"
            self.settings.compiler.version = "4.9"
            self.settings.compiler.libcxx = "libstdc++11"

    # def configure(self):
    #     if self.settings.compiler == 'Visual Studio':
    #         del self.options.fPIC

    def build_requirements(self):
        if self.settings.os == "Windows":
            self.build_requires("mingw_lncc/4.9.4@Promob3D_Dependencias/stable")
        #     self.settings.compiler='intel'
        #     self.settings.compiler.version=19
        #     self.settings.compiler.base='Visual Studio'
        #     self.settings.compiler.version=14

        if self.options.metis:
            self.build_requires("metis_lncc/5.1.0@Promob3D_Dependencias/stable")

    def build(self):
        cmake = CMake(self, generator="Ninja")
        # cmake.verbose = True
        # cmake.parallel = False
        cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = self.options.fPIC
        cmake.definitions["CONAN_DISABLE_CHECK_COMPILER"] = "ON"

        if self.options.parallel:
            cmake.definitions["parallel"] = "ON"
        else:
            cmake.definitions["parallel"] = "OFF"

        if self.options.autobuild:
            cmake.definitions["autobuild"] = "ON"
        else:
            cmake.definitions["autobuild"] = "OFF"

        if self.options.openmp:
            cmake.definitions["openmp"] = "ON"
        else:
            cmake.definitions["openmp"] = "OFF"

        if self.options.metis:
            cmake.definitions["metis"] = "ON"
        else:
            cmake.definitions["metis"] = "OFF"

        if self.options.scotch:
            cmake.definitions["scotch"] = "ON"
        else:
            cmake.definitions["scotch"] = "OFF"

        cmake.configure(source_folder=self.unzipFolder)
        cmake.build()


    def package(self):
        package_progrmas = ['Csimple', 'd_simple', 'mumpscfg', 's_simple']
        for i in package_progrmas:
            self.copy(i,src="bin", dst="bin", keep_path=False)
        self.copy("*.h", dst="include", keep_path=False)
        self.copy("*mumps.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["dmumps", "mpiseq", "mumps_common", "smumps", "pord", "gfortran"]


