######################################################################
#
# CMakeLists.txt for SUPERLU
#
######################################################################

# Required version
cmake_minimum_required(VERSION 2.8.12)

# Project Version	
project(SuperLUMT "C" "Fortran")
# project(SuperLUMT "C")

message(STATUS "CMAKE_BINARY_DIR: "${CMAKE_BINARY_DIR})

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

set(VERSION_MAJOR "3")
set(VERSION_MINOR "1")
set(PROJECT_VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_BugFix})

######################################################################
#
# IDEAS: xSDK standards module
# MESSAGE("\nProcess XSDK defaults ...")
# SET(USE_XSDK_DEFAULTS_DEFAULT TRUE) # Set to false if desired
# INCLUDE("cmake/XSDKDefaults.cmake")
######################################################################

######################################################################
#
# Usual initialization stuff
#
######################################################################
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)    ## ????
set(CMAKE_INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib")

if (BUILD_SHARED_LIBS)
  message("-- SuperLUMT will be built as a shared library.")
  set(PROJECT_NAME_LIB_EXPORT libsuperlumt.so)
else()
  message("-- SuperLUMT will be built as a static library.")
  set(PROJECT_NAME_LIB_EXPORT libsuperlumt.a)
endif()

enable_language(C)
#enable_language (Fortran)
set(NOFORTRAN TRUE)
set(SUPERLU_VERSION "${PROJECT_VERSION}")
set(SUPERLU_REV "${PROJECT_REV}")

# The XSDK standard does not allow using internally built BLAS
# if (USE_XSDK_DEFAULTS)
set(enable_blaslib_DEFAULT OFF)
# else()
# set(enable_blaslib_DEFAULT ON)
# endif()

# setup options
option(enable_blaslib   "Build the CBLAS library" ${enable_blaslib_DEFAULT})
option(enable_matlabmex "Build the Matlab mex library" OFF)
option(enable_tests     "Build tests" OFF)
option(enable_doc       "Build doxygen documentation" OFF)
option(enable_single    "Enable single precision library" ON)
option(enable_double    "Enable double precision library" ON)
option(enable_complex   "Enable complex precision library" ON)
option(enable_complex16 "Enable complex16 precision library" ON)
# option(enable_examples  "Build examples" ON)

find_package(OpenMP REQUIRED)
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}" CACHE STRING "CMake C flags updated with OpenMP" FORCE)

# setup required compiler defines and options.
## add_definitions(-DPRNTlevel=0 -DAdd_)
## get_directory_property( DirDefs COMPILE_DEFINITIONS )
set(CMAKE_C_FLAGS "-DMACH=OPENMP ${CMAKE_C_FLAGS}")
set(CMAKE_C_FLAGS "-DPRNTlevel=0 -DAdd_ ${CMAKE_C_FLAGS}")
set(CMAKE_C_FLAGS_RELEASE "-O3" CACHE STRING "")

######################################################################
#
# Find packages
#
######################################################################
#
#--------------------- BLAS ---------------------
# if(NOT enable_blaslib)
#   if (TPL_BLAS_LIBRARIES)
#     set(BLAS_FOUND TRUE)
#   else()
#     # find_package(BLAS)
#     find_package(lapack)
#     if (BLAS_FOUND)
#       set(TPL_BLAS_LIBRARIES "${BLAS_LIBRARIES}" CACHE FILEPATH
#         "Set from FindBLAS.cmake BLAS_LIBRARIES." FORCE)
#     endif()
#   endif()
# endif()

# if(BLAS_FOUND)
#   message("-- Using TPL_BLAS_LIBRARIES='${TPL_BLAS_LIBRARIES}'")
#   set(CMAKE_C_FLAGS "-DUSE_VENDOR_BLAS ${CMAKE_C_FLAGS}")
#   set(BLAS_LIB ${TPL_BLAS_LIBRARIES})
#   # fix up BLAS library name
#   string (REPLACE ";" " " BLAS_LIB_STR "${BLAS_LIB}")
#   set(BLAS_LIB_EXPORT ${BLAS_LIB_STR})
# else()
#   message("-- Did not find or specify BLAS so configure to build internal CBLAS ...")
#   add_subdirectory(CBLAS)
#   set(BLAS_LIB blas)
#   if (BUILD_SHARED_LIBS)  # export to be referenced by downstream makefile
#       set(BLAS_LIB_EXPORT ${CMAKE_SOURCE_DIR}/build/CBLAS/libblas.so)
#   else()
#       set(BLAS_LIB_EXPORT ${CMAKE_SOURCE_DIR}/build/CBLAS/libblas.a)
#   endif()
# endif()

# find_package(cblas)



######################################################################
#
# Include directories
#
######################################################################

include_directories(${CONAN_INCLUDE_DIRS})
include_directories(${CMAKE_SOURCE_DIR}/SRC)

######################################################################
#
# Add subdirectories
#
######################################################################

# add_subdirectory(SRC)

set(headers
colamd.h
pxgstrf_synch.h
slu_dcomplex.h
slu_mt_cdefs.h
slu_mt_Cnames.h
slu_mt_ddefs.h
slu_mt_machines.h
slu_mt_sdefs.h
slu_mt_util.h
slu_mt_zdefs.h
slu_scomplex.h
supermatrix.h)

set(sources
SRC/await.c
SRC/cgscon.c
SRC/cgsequ.c
SRC/cgsrfs.c
SRC/cgstrs.c
SRC/cholnzcnt.c
SRC/clacon.c
SRC/clangs.c
SRC/claqgs.c
SRC/cmatgen.c
SRC/cmyblas2.c
SRC/colamd.c
SRC/cpivotgrowth.c
SRC/creadhb.c
SRC/creadmt.c
SRC/creadrb.c
SRC/csp_blas2.c
SRC/csp_blas3.c
SRC/dclock.c
SRC/dcomplex.c
SRC/dgscon.c
SRC/dgsequ.c
SRC/dgsrfs.c
SRC/dgstrs.c
SRC/dlacon.c
SRC/dlamch.c
SRC/dlangs.c
SRC/dlaqgs.c
SRC/dmatgen.c
SRC/dmyblas2.c
SRC/dpivotgrowth.c
SRC/dreadhb.c
SRC/dreadmt.c
SRC/dreadrb.c
SRC/dsp_blas2.c
SRC/dsp_blas3.c
SRC/dzsum1.c
SRC/get_perm_c.c
SRC/heap_relax_snode.c
SRC/icmax1.c
SRC/izmax1.c
SRC/lsame.c
SRC/mmd.c
SRC/pcgssv.c
SRC/pcgssvx.c
SRC/pcgstrf_bmod1D.c
SRC/pcgstrf_bmod1D_mv2.c
SRC/pcgstrf_bmod2D.c
SRC/pcgstrf_bmod2D_mv2.c
SRC/pcgstrf.c
SRC/pcgstrf_column_bmod.c
SRC/pcgstrf_column_dfs.c
SRC/pcgstrf_copy_to_ucol.c
SRC/pcgstrf_factor_snode.c
SRC/pcgstrf_init.c
SRC/pcgstrf_panel_bmod.c
SRC/pcgstrf_panel_dfs.c
SRC/pcgstrf_pivotL.c
SRC/pcgstrf_snode_bmod.c
SRC/pcgstrf_snode_dfs.c
SRC/pcgstrf_thread.c
SRC/pcgstrf_thread_finalize.c
SRC/pcgstrf_thread_init.c
SRC/pcmemory.c
SRC/pcutil.c
SRC/pdgssv.c
SRC/pdgssvx.c
SRC/pdgstrf_bmod1D.c
SRC/pdgstrf_bmod1D_mv2.c
SRC/pdgstrf_bmod2D.c
SRC/pdgstrf_bmod2D_mv2.c
SRC/pdgstrf.c
SRC/pdgstrf_column_bmod.c
SRC/pdgstrf_column_dfs.c
SRC/pdgstrf_copy_to_ucol.c
SRC/pdgstrf_factor_snode.c
SRC/pdgstrf_init.c
SRC/pdgstrf_panel_bmod.c
SRC/pdgstrf_panel_dfs.c
SRC/pdgstrf_pivotL.c
SRC/pdgstrf_snode_bmod.c
SRC/pdgstrf_snode_dfs.c
SRC/pdgstrf_thread.c
SRC/pdgstrf_thread_finalize.c
SRC/pdgstrf_thread_init.c
SRC/pdmemory.c
SRC/pdutil.c
SRC/pmemory.c
SRC/psgssv.c
SRC/psgssvx.c
SRC/psgstrf_bmod1D.c
SRC/psgstrf_bmod1D_mv2.c
SRC/psgstrf_bmod2D.c
SRC/psgstrf_bmod2D_mv2.c
SRC/psgstrf.c
SRC/psgstrf_column_bmod.c
SRC/psgstrf_column_dfs.c
SRC/psgstrf_copy_to_ucol.c
SRC/psgstrf_factor_snode.c
SRC/psgstrf_init.c
SRC/psgstrf_panel_bmod.c
SRC/psgstrf_panel_dfs.c
SRC/psgstrf_pivotL.c
SRC/psgstrf_snode_bmod.c
SRC/psgstrf_snode_dfs.c
SRC/psgstrf_thread.c
SRC/psgstrf_thread_finalize.c
SRC/psgstrf_thread_init.c
SRC/psmemory.c
SRC/psutil.c
SRC/pxgstrf_finalize.c
SRC/pxgstrf_mark_busy_descends.c
SRC/pxgstrf_pruneL.c
SRC/pxgstrf_relax_snode.c
SRC/pxgstrf_scheduler.c
SRC/pxgstrf_super_bnd_dfs.c
SRC/pxgstrf_synch.c
SRC/pzgssv.c
SRC/pzgssvx.c
SRC/pzgstrf_bmod1D.c
SRC/pzgstrf_bmod1D_mv2.c
SRC/pzgstrf_bmod2D.c
SRC/pzgstrf_bmod2D_mv2.c
SRC/pzgstrf.c
SRC/pzgstrf_column_bmod.c
SRC/pzgstrf_column_dfs.c
SRC/pzgstrf_copy_to_ucol.c
SRC/pzgstrf_factor_snode.c
SRC/pzgstrf_init.c
SRC/pzgstrf_panel_bmod.c
SRC/pzgstrf_panel_dfs.c
SRC/pzgstrf_pivotL.c
SRC/pzgstrf_snode_bmod.c
SRC/pzgstrf_snode_dfs.c
SRC/pzgstrf_thread.c
SRC/pzgstrf_thread_finalize.c
SRC/pzgstrf_thread_init.c
SRC/pzmemory.c
SRC/pzutil.c
SRC/qrnzcnt.c
SRC/scomplex.c
SRC/scsum1.c
SRC/sgscon.c
SRC/sgsequ.c
SRC/sgsrfs.c
SRC/sgstrs.c
SRC/slacon.c
SRC/slamch.c
SRC/slangs.c
SRC/slaqgs.c
SRC/smatgen.c
SRC/smyblas2.c
SRC/sp_coletree.c
SRC/sp_colorder.c
SRC/sp_ienv.c
SRC/spivotgrowth.c
SRC/sreadhb.c
SRC/sreadmt.c
SRC/sreadrb.c
SRC/ssp_blas2.c
SRC/ssp_blas3.c
SRC/superlu_timer.c
SRC/util.c
SRC/xerbla.c
SRC/zgscon.c
SRC/zgsequ.c
SRC/zgsrfs.c
SRC/zgstrs.c
SRC/zlacon.c
SRC/zlangs.c
SRC/zlaqgs.c
SRC/zmatgen.c
SRC/zmyblas2.c
SRC/zpivotgrowth.c
SRC/zreadhb.c
SRC/zreadmt.c
SRC/zreadrb.c
SRC/zsp_blas2.c
SRC/zsp_blas3.c)

set_source_files_properties(superlu_timer.c PROPERTIES COMPILE_FLAGS -O0)

set_source_files_properties(smach.c PROPERTIES COMPILE_FLAGS -O0)

set_source_files_properties(dmach.c PROPERTIES COMPILE_FLAGS -O0)

add_library(superlumt ${sources} ${HEADERS})



# message(STATUS "\t***SRC CONAN_INCLUDE_DIRS: " ${CONAN_INCLUDE_DIRS})
# message(STATUS "\t***SRC CONAN_FRAMEWORKS_LAPACK: " ${CONAN_FRAMEWORKS_LAPACK})
# message(STATUS "\t***SRC CONAN_LIBS: " ${CONAN_LIBS})
# message(STATUS "\t***SRC CONAN_LIBS_DIR: " ${CONAN_LIBS_DIR})
# message(STATUS "\t***SRC BLAS_LIB: " ${BLAS_LIB})

# target_link_libraries(exe_using_avcodec PRIVATE CONAN_PKG::ffmpeg::libavcodec)

# target_link_libraries(superlumt ${BLAS_LIB} m ${CONAN_LIBS})
target_link_libraries(superlumt ${CONAN_LIBS})

set_target_properties(superlumt PROPERTIES
  VERSION ${PROJECT_VERSION} SOVERSION ${VERSION_MAJOR}
  )

include(GNUInstallDirs)

install(TARGETS superlumt
#  DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
  DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

install(FILES ${headers}
#  DESTINATION ${CMAKE_INSTALL_PREFIX}/include
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)



# if(enable_matlabmex)
#   add_subdirectory(MATLAB)
# endif()

# if(enable_tests)
#   enable_testing()
#   add_subdirectory(TESTING)
# endif()

# if(enable_doc)
#   message(FATAL_ERROR "Documentation build requested but not implemented.")
#   #implement doxygen
# endif()


file(WRITE "make.defs" "# can be exposed to users" ${CMAKE_C_COMPILER}  )
configure_file(${CMAKE_SOURCE_DIR}/make.inc.in ${CMAKE_SOURCE_DIR}/make.inc)
 

