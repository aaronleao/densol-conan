#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import shutil
import glob
from conans import ConanFile, CMake, tools
from conans.model.version import Version
from conans.errors import ConanException
from conans.tools import SystemPackageTool

# python 2 / 3 StringIO import
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from conans.errors import ConanInvalidConfiguration


class SuperLUConan(ConanFile):
    name = "superlu_lncc"
    version = "5.2.1"
    license = "LICENSE"
    homepage = "https://portal.nersc.gov/project/sparse/superlu/"
    description = "Fortran subroutines for solving problems in numerical linear algebra"
    url = "https://portal.nersc.gov/project/sparse/superlu/"
    settings = "os", "arch", "compiler", "build_type"
    options = {"shared": [True, False], "fPIC": [True, False], "visual_studio": [True, False]}
    default_options = {"shared": False, "visual_studio": False, "fPIC": True}
    exports = "slu.zip"
    generators = "cmake"
    requires = "ninja-kitware/1.7.2@Promob3D_Dependencias/stable",\
    "cmake_lncc/3.13.4@Promob3D_Dependencias/stable",\
    "lapack_lncc/3.6.1@Promob3D_Dependencias/stable"
    _source_subfolder = "_source_subfolder"
    _build_subfolder = "build_subfolder"
    user = "Promob3D_Dependencias"
    channel = "stable"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def build_requirements(self):
        if self.settings.os == 'Windows':
                self.build_requires("mingw_lncc/4.9.4@Promob3D_Dependencias/stable")

    def configure(self):
        if self.settings.os == "Windows":
            self.settings.compiler = "gcc"
            self.settings.compiler.version = "4.9"
            self.settings.compiler.libcxx = "libstdc++11"

    def export(self):
        self.copy("CMakeLists.txt",  self._source_subfolder, keep_path=False)
        self.copy("make.inc.in",     self._source_subfolder, keep_path=False)

    def source(self):
        tools.unzip("slu.zip" )
        os.rename("{}".format("slu"), self._source_subfolder)

    def build(self):
        cmake = CMake(self, generator="Ninja")
        # cmake.verbose = False
        # cmake.parallel = True
        cmake.definitions["CONAN_DISABLE_CHECK_COMPILER"] = True
        cmake.definitions["enable_blaslib"] = False
        cmake.definitions["enable_tests"] = False
        cmake.configure(source_folder=self._source_subfolder)
        cmake.build()

    def package_info(self):
        self.cpp_info.includedirs = ['include']
        self.cpp_info.libs = ["superlu"]

    def package(self):
        self.copy("LICENSE", dst="licenses")
        self.copy(os.path.join(self._source_subfolder, "SRC", "*.h"), dst="include", keep_path=False)
        self.copy("*superlumt.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
