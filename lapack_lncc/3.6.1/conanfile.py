#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import shutil
import glob
from conans import ConanFile, CMake, tools
from conans.model.version import Version
from conans.errors import ConanException
from conans.tools import SystemPackageTool
import time
# python 2 / 3 StringIO import
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from conans.errors import ConanInvalidConfiguration


class LapackConan(ConanFile):
    name = "lapack_lncc"
    version = "3.6.1"
    license = "BSD-3-Clause"
    homepage = "https://github.com/Reference-LAPACK/lapack"
    description = "Fortran subroutines for solving problems in numerical linear algebra"
    url = "https://github.com/conan-community/conan-lapack"
    settings = "os", "arch", "compiler", "build_type"
    options = {"shared": [True, False], "fPIC": [True, False], "visual_studio": [True, False]}
    default_options = {"shared": False, "visual_studio": False, "fPIC": True}
    exports = "LICENSE"
    exports_sources = "CMakeLists.txt", "lapack-3.6.1.zip"
    generators = "cmake"
    requires = "zlib_lncc/1.2.11@Promob3D_Dependencias/stable",\
    "ninja-kitware/1.7.2@Promob3D_Dependencias/stable",\
    "cmake_lncc/3.13.4@Promob3D_Dependencias/stable"
    _source_subfolder = "source_subfolder"
    _build_subfolder = "build_subfolder"
    user = "Promob3D_Dependencias"
    channel = "stable"
    fileName = "lapack-3.6.1.zip"
    

    def configure(self):
        if self.settings.os == "Windows":
            self.settings.compiler = "gcc"
            self.settings.compiler.version = "4.9"
            self.settings.compiler.libcxx = "libstdc++11"

    def build_requirements(self):
        self.build_requires("zlib_lncc/1.2.11@Promob3D_Dependencias/stable")
        if self.settings.os == 'Windows':
                self.build_requires("mingw_lncc/4.9.4@Promob3D_Dependencias/stable")

        if self.settings.os == 'Linux':
            if not tools.which('pkg-config'):
                self.build_requires('pkg-config_installer/0.29.2@bincrafters/stable')

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        # tools.get("{}/archive/v{}.zip".format(self.homepage, self.version))
        tools.unzip(os.path.join(self.source_folder,self.fileName))
        # time.sleep(5)
        os.rename("{}-{}".format("lapack", self.version), self._source_subfolder)

    # def build_requirements(self):
    #     if self.settings.os == "Windows":
    #         self.build_requires("mingw_installer/1.0@conan/stable")

    # def system_requirements(self):        
    #     if tools.which("gfortran") is not None:
    #         # Prefere local install
    #         return
    #     installer = SystemPackageTool()
    #     if tools.os_info.is_linux:
    #         if tools.os_info.with_zypper or tools.os_info.with_pacman:
    #             installer.install("gcc-fortran")
    #         elif tools.os_info.with_yum:
    #             installer.install("gcc-gfortran")
    #         else:
    #             installer.install("gfortran")
    #             versionfloat = Version(self.settings.compiler.version.value)
    #             if self.settings.compiler == "gcc":
    #                 if versionfloat < "5.0":
    #                     installer.install("libgfortran-{}-dev".format(versionfloat))
    #                 else:
    #                     installer.install("libgfortran-{}-dev".format(int(versionfloat)))
    #     if tools.os_info.is_macos and Version(self.settings.compiler.version.value) > "7.3":
    #         try:
    #             installer.install("gcc", update=True, force=True)
    #         except Exception:
    #             self.output.warn("brew install gcc failed. Tying to fix it with 'brew link'")
    #             self.run("brew link --overwrite gcc")

    def _configure_cmake(self):
        cmake = CMake(self, generator="Ninja")
        cmake.definitions["CONAN_DISABLE_CHECK_COMPILER"] = True
        # cmake.definitions["CMAKE_GNUtoMS"] = self.options.visual_studio
        cmake.definitions["BUILD_TESTING"] = False

        if self.settings.os == "Windows":
            cmake.definitions["LAPACKE"] = True
            cmake.definitions["CBLAS"] = True
            # cmake.definitions["CMAKE_Fortran_COMPILER"] = fortranCompiler
        if self.settings.os == "Linux":
            cmake.definitions["LAPACKE"] = True
            cmake.definitions["CBLAS"] = True
        cmake.configure(build_dir=self._build_subfolder)
        return cmake

    def build(self):
        # if self.settings.compiler == "Visual Studio":
        #     raise ConanInvalidConfiguration("This library cannot be built with Visual Studio. Please use MinGW to "
        #                     "build it and option 'visual_studio=True' to build and consume.")
        cmake = self._configure_cmake()
        if self.settings.os == "Windows":
            # for target in ["blas", "lapack"]:
            for target in ["blas", "cblas", "lapack", "lapacke"]:
                cmake.build(target=target)
        if self.settings.os == "Linux":
            for target in ["blas", "cblas", "lapack", "lapacke"]:
                cmake.build(target=target)
                
    def package(self):
        self.copy(pattern="LICENSE", dst="licenses", src=self._source_subfolder)
        cmake = self._configure_cmake()
        cmake.install()

        if self.options.visual_studio:
            for bin_path in self.deps_cpp_info.bin_paths:  # Copy MinGW dlls for Visual Studio consumers
                self.copy(pattern="*seh*.dll", dst="bin", src=bin_path, keep_path=False)
                self.copy(pattern="*sjlj*.dll", dst="bin", src=bin_path, keep_path=False)
                self.copy(pattern="*dwarf2*.dll", dst="bin", src=bin_path, keep_path=False)
                self.copy(pattern="*quadmath*.dll", dst="bin", src=bin_path, keep_path=False)
                self.copy(pattern="*winpthread*.dll", dst="bin", src=bin_path, keep_path=False)
                self.copy(pattern="*gfortran*.dll", dst="bin", src=bin_path, keep_path=False)

            with tools.chdir(os.path.join(self.package_folder, "lib")):
                libs = glob.glob("lib*.a")
                for lib in libs:
                    vslib = lib[3:-2] + ".lib"
                    self.output.info('renaming %s into %s' % (lib, vslib))
                    shutil.move(lib, vslib)

    def package_id(self):
        if self.options.visual_studio:
            self.info.settings.compiler = "Visual Studio"
            del self.info.settings.compiler.version
            del self.info.settings.compiler.runtime
            del self.info.settings.compiler.toolset

    def package_info(self):
        # the order is important for static builds
        if tools.os_info.is_linux:
            self.cpp_info.libs = ["lapacke", "lapack", "blas", "cblas", "quadmath", "m"]
        if tools.os_info.is_windows:
            self.cpp_info.libs = ["lapack", "blas", "quadmath"]
        if self.options.visual_studio and self.options.shared:
            self.cpp_info.libs = ["lapacke.dll.lib", "lapack.dll.lib", "blas.dll.lib", "cblas.dll.lib"]
        self.cpp_info.libdirs = ["lib"]
        if tools.os_info.is_macos:
            brewout = StringIO()
            try:
                self.run("gfortran --print-file-name libgfortran.dylib", output=brewout)
            except Exception as error:
                raise ConanException("Failed to run command: {}. Output: {}".format(error, brewout.getvalue()))
            lib = os.path.dirname(os.path.normpath(brewout.getvalue().strip()))
            self.cpp_info.libdirs.append(lib)
