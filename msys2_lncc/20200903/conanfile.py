from conans import ConanFile, tools
from conans.errors import ConanInvalidConfiguration
import os
import shutil
from six.moves.urllib.parse import urlparse

class MSYS2Conan(ConanFile):
    name = "msys2_lncc"
    version = "20200903"
    description = "MSYS2 is a software distro and building platform for Windows"
    #url = "https://github.com/conan-io/conan-center-index"
    # url = "http://repo.msys2.org/distrib/x86_64/msys2-base-x86_64-20200517.tar.xz"
    homepage = "http://www.msys2.org"
    license = "MSYS license"
    topics = ("conan", "msys", "unix", "subsystem")
    build_requires = "7z_lncc/16.02@Promob3D_Dependencias/stable"
    short_paths = False
    fileName = "msys2-base-x86_64-%s.tar.xz"%(version)
    options = {"exclude_files": "ANY",  # Comma separated list of file patterns to exclude from the package
               "packages": "ANY",  # Comma separated
               "additional_packages": "ANY"}    # Comma separated
    default_options = {"exclude_files": "*/link.exe",
                       "packages": "base-devel,binutils,gcc",
                       "additional_packages": None}
    settings = "os_build", "arch_build"
    user = "Promob3D_Dependencias"
    channel = "stable"
    exports_sources = "msys2-base-x86_64-%s.tar.xz"%(version), "mirrorlist.mingw32", "mirrorlist.mingw64", "mirrorlist.msys", "PKGBUILD"

    def configure(self):
        if self.settings.os_build != "Windows":
            raise ConanInvalidConfiguration("Only Windows supported")

    def source(self):
        # build tools have to download files in build method when the
        # source files downloaded will be different based on architecture or OS
        pass

    #def _download(self, url, sha256):
    #    from six.moves.urllib.parse import urlparse
    #    filename = os.path.basename(urlparse(url[0]).path)
    #    tools.download(url=url, filename=filename, sha256=sha256)
    #    return filename

    @property
    def _msys_dir(self):
        return "msys64" if self.settings.arch_build == "x86_64" else "msys32"

    def build(self):
        arch = 0 if self.settings.arch_build == "x86" else 1  # index in the sources list
        #filename = os.path.basename(urlparse(url[0]).path)
        #filename = self._download(**self.conan_data["sources"][self.version][arch])
        # tools.download(url=self.url, filename=self.fileName)
        tar_name = self.fileName.replace(".xz", "")
        self.run("7z.exe x %s"%(os.path.join(self.source_folder, self.fileName)))
        self.run("7z.exe x %s"%(tar_name))
        #os.unlink(self.fileName)
        #os.unlink(tar_name)


        shutil.copy(os.path.join(self.source_folder,"mirrorlist.mingw32" ), os.path.join(self._msys_dir, "etc", "pacman.d"))
        shutil.copy(os.path.join(self.source_folder,"mirrorlist.mingw64" ), os.path.join(self._msys_dir, "etc", "pacman.d"))
        shutil.copy(os.path.join(self.source_folder,"mirrorlist.msys" ), os.path.join(self._msys_dir, "etc", "pacman.d"))

        packages = []
        if self.options.packages:
            packages.extend(str(self.options.packages).split(","))
        if self.options.additional_packages:
            packages.extend(str(self.options.additional_packages).split(","))

        with tools.chdir(os.path.join(self._msys_dir, "usr", "bin")):
            self.run('bash -l -c "pacman -Sy --noconfirm"')
            self.run('bash -l -c "pacman --needed -S bash pacman msys2-runtime --noconfirm"')
            self.run('bash -l -c "pacman -Su --noconfirm"')


        with tools.chdir(os.path.join(self._msys_dir, "usr", "bin")):
            for package in packages:
                self.run('bash -l -c "pacman -S %s --noconfirm"' % package)

        # create /tmp dir in order to avoid
        # bash.exe: warning: could not find /tmp, please create!
        tmp_dir = os.path.join(self._msys_dir, 'tmp')
        if not os.path.isdir(tmp_dir):
            os.makedirs(tmp_dir)
        tmp_name = os.path.join(tmp_dir, 'dummy')
        with open(tmp_name, 'a'):
            os.utime(tmp_name, None)

        # Prepend the PKG_CONFIG_PATH environment variable with an eventual PKG_CONFIG_PATH environment variable
        tools.replace_in_file(os.path.join(self._msys_dir, "etc", "profile"),
                              'PKG_CONFIG_PATH="', 'PKG_CONFIG_PATH="$PKG_CONFIG_PATH:')

    def package(self):
        excludes = None
        if self.options.exclude_files:
            excludes = tuple(str(self.options.exclude_files).split(","))
        self.copy("*", dst="bin", src=self._msys_dir, excludes=excludes)
        shutil.copytree(os.path.join(self.package_folder, "bin", "usr", "share", "licenses"),
                        os.path.join(self.package_folder, "licenses"))

    def package_info(self):
        msys_root = os.path.join(self.package_folder, "bin")
        msys_bin = os.path.join(msys_root, "usr", "bin")

        self.output.info("Creating MSYS_ROOT env var : %s" % msys_root)
        self.env_info.MSYS_ROOT = msys_root

        self.output.info("Creating MSYS_BIN env var : %s" % msys_bin)
        self.env_info.MSYS_BIN = msys_bin

        self.output.info("Appending PATH env var with : " + msys_bin)
        self.env_info.path.append(msys_bin)
