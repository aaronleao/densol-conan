import os
from conans import ConanFile, AutoToolsBuildEnvironment, tools, MSBuild
import shutil

class GperftoolsConan(ConanFile):
    name = "gperftools_lncc"
    version = "2.7"
    license = "BSD-3-Clause"
    url = "https://github.com/Aetf/salus-ci"
    homepage = "https://github.com/gperftools/gperftools"
    description = ("The fastest malloc we've seen; works particularly well with threads and STL."
                   "Also: thread-friendly heap-checker, heap-profiler, and cpu-profiler.")
    settings = "os", "compiler", "build_type", "arch"
    short_paths = False
    exports_sources = 'src/*', 'gperftools.sln', 'vsprojects/*', 'gperftools-2.7.tar.gz'
    user = "Promob3D_Dependencias"
    channel = "stable"
    options = {"visual_studio": [True, False] }
    default_options = "visual_studio=False"
    fileName = "gperftools-2.7.tar.gz"

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def build_requirements(self):
        if self.settings.os == "Windows":
            self.build_requires("mingw_lncc/4.9.4@Promob3D_Dependencias/stable")
            self.build_requires("msys2_lncc/20200903@Promob3D_Dependencias/stable")

    def configure(self):
        if self.settings.os == "Windows":
            self.settings.compiler = "gcc"
            self.settings.compiler.version = "4.9"
            self.settings.compiler.libcxx = "libstdc++11"

    def source(self):
        # tools.get(**self.conan_data["sources"][self.version])
        tools.unzip(os.path.join(self.source_folder,self.fileName))
        extracted_dir = "gperftools-2.7"
        os.rename(extracted_dir, self._source_subfolder)
        if self.settings.os == "Windows":
            shutil.rmtree(os.path.join(self._source_subfolder, "vsprojects"))
            shutil.copy("gperftools.sln",self._source_subfolder)
            shutil.move("vsprojects", self._source_subfolder)

    _autotools = False

    def _configure_autotools(self):
        if not self._autotools:
            self._autotools = AutoToolsBuildEnvironment(self, win_bash=tools.os_info.is_windows)
            env_build_vars = self._autotools.vars
            # env_build_vars['CONAN_CPU_COUNT'] = '1'
            args = ['--disable-shared', '--enable-static', '--disable-libunwind']
            self._autotools.fpic=True
            with tools.chdir(self._source_subfolder):
                self._autotools.configure(args=args, vars=env_build_vars)

        return self._autotools

    def buildPosix(self):
        autotools = self._configure_autotools()

        with tools.chdir(self._source_subfolder):
            autotools.make()

    def buildWindows(self):
        msbuild = MSBuild(self)
        msbuild.build(os.path.join(self._source_subfolder, "gperftools.sln"),upgrade_project=False, build_type="Release")

    def build(self):
        # if self.settings.os == "Windows" and self.otptions.visual_studio:
        #    self.buildWindows()
        # else:
        self.buildPosix()

    def package(self):
        autotools = self._configure_autotools()
        with tools.chdir(self._source_subfolder):
            autotools.install()
        self.copy("*.cmake", src="src")
        with tools.chdir(os.path.join(self.package_folder, "lib")):
            self.run("rm -f *.la")

    def package_info(self):
        # self.cpp_info.libs = ["tcmalloc", "tcmalloc_minimal", "profiler"]
        self.cpp_info.libs = ["tcmalloc_minimal"]
        if self.settings.os == "Linux":
            self.cpp_info.libs.append("pthread")
