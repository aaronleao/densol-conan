# -*- coding: utf-8 -*-
import os
from conans import tools, ConanFile, CMake
from conans import __version__ as conan_version
from conans.model.version import Version
from conans.errors import ConanInvalidConfiguration, NotFoundException, ConanException

class CMakeInstallerConan(ConanFile):
    name = "cmake_lncc"
    version = "3.13.4"
    license = "BSD 3-clause"
    author = "Aaron aaron@lncc.br"
    url = "https://cmake.org/files/v3.13/"
    description = "Ninja is a small build system with a focus on speed. https://ninja-build.org/"
    topics = ("builder", "minimalist", "fast")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "virtualrunenv" 
    exports_sources = "cmake-3.13.4-Linux-x86_64.tar.gz", "cmake-3.13.4-win64-x64.zip"
    user = "Promob3D_Dependencias"
    channel = "stable"
    fileName = ""

    def _minor_version(self):
        return ".".join(str(self.version).split(".")[:2])

    def _minor_version(self):
        return ".".join(str(self.version).split(".")[:2])

    def source(self):
        if tools.os_info.is_linux:
            self.fileName="cmake-%s-Linux-x86_64.tar.gz"%(self.version)
        if tools.os_info.is_windows:
            self.fileName="cmake-%s-win64-x64.zip"%( self.version)
            self.unzipFolder = "cmake-%s-win64-x64"%(self.version)
        # tools.get(self.fileName)

        tools.unzip(os.path.join(self.source_folder,self.fileName))



    def package(self):
        if tools.os_info.is_linux:
            sourceFolder=os.path.join(self.source_folder, "cmake-%s-Linux-x86_64"%(self.version))
        if tools.os_info.is_windows:
            sourceFolder=os.path.join(self.source_folder, "cmake-%s-win64-x64"%(self.version))
        self.copy("*", src=os.path.join(sourceFolder, "bin"), dst="bin")
        self.copy("*", src=os.path.join(sourceFolder, "man"), dst="man")
        self.copy("*", src=os.path.join(sourceFolder, "share"), dst="share")
        self.copy("activate_run*", src=self.build_folder, dst=".")
        self.copy("deactivate_run*", src=self.build_folder, dst=".")
        self.copy("environment*", src=self.build_folder, dst=".")

    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        minor = self._minor_version()
        self.env_info.CMAKE_ROOT = self.package_folder
        mod_path = os.path.join(self.package_folder, "share", "cmake-%s" % minor, "Modules")
        self.env_info.CMAKE_MODULE_PATH = mod_path
        if not os.path.exists(mod_path):
            raise ConanException("Module path not found: %s" % mod_path)
